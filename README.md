Dynamic Time Warping dissimilarity measure adaptation to on-line
learning settings.

---

# Citation

```
@inproceedings{oregi2017,
  title={On-Line Dynamic Time Warping for Streaming Time Series},
  author={Oregi, Izaskun and Pérez, Aritz and Del Ser, Javier and Lozano, José A},
  booktitle={Joint European Conference on Machine Learning and Knowledge Discovery in Databases},
  pages={591--605},
  year={2017},
  organization={Springer}
}
```